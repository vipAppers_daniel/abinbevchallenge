//
//  Product.swift
//  abinbevChallenge
//
//  Created by Station_02 on 01/08/20.
//  Copyright © 2020 Vip Appers. All rights reserved.
//

import UIKit
import AVFoundation


class Product: NSObject, NSCoding {
    
    // MARK: - Properties
    struct PropertyKey {
        static let prodPhotoKey         = "prodPhoto"
        static let prodDescriptionKey   = "prodDescription"
        static let boxUnitKey           = "boxUnit"
        static let boxDescriptionKey    = "boxDescription"
        static let qtdProductkey        = "qtdProduct"
        static let qtdDescriptionKey    = "qtdDescription"
        static let priceKey             = "price"
        static let priceDiscountKey     = "priceDiscount"
        static let recycleKey           = "recycle"
        static let clientQtdKey         = "clientQtd"
        static let clientQtdStateKey    = "clientQtdState"
        }
    
    var prodPhoto        : UIImage
    var prodDescription  : String
    var boxUnit          : String
    var boxDescription   : String
    var qtdProduct       : String
    var qtdDescription   : String
    var price            : String
    var priceDiscount    : String
    var recycle          : String
    var clientQtd        : String
    var clientQtdState   : String
    
    
  
    // MARK: - Initialization
    init?(prodPhoto: UIImage, prodDescription: String, boxUnit: String, boxDescription: String, qtdProduct: String, qtdDescription: String, price: String, priceDiscount: String, recycle: String, clientQtd: String, clientQtdState: String) {
        
        self.prodPhoto       = prodPhoto
        self.prodDescription = prodDescription
        self.boxUnit         = boxUnit
        self.boxDescription  = boxDescription
        self.qtdProduct      = qtdProduct
        self.qtdDescription  = qtdDescription
        self.price           = price
        self.priceDiscount   = priceDiscount
        self.recycle         = recycle
        self.clientQtd       = clientQtd
        self.clientQtdState  = clientQtdState
        
        super.init()
        
        if prodDescription.isEmpty || price.isEmpty {
            return nil
        }
    }
    
    // MARK: - NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(prodPhoto, forKey: PropertyKey.prodPhotoKey)
        aCoder.encode(prodDescription, forKey: PropertyKey.prodDescriptionKey)
        aCoder.encode(boxUnit, forKey: PropertyKey.boxUnitKey)
        aCoder.encode(boxDescription, forKey: PropertyKey.boxDescriptionKey)
        aCoder.encode(qtdProduct, forKey: PropertyKey.qtdProductkey)
        aCoder.encode(qtdDescription, forKey: PropertyKey.qtdDescriptionKey)
        aCoder.encode(price, forKey: PropertyKey.priceKey)
        aCoder.encode(priceDiscount, forKey: PropertyKey.priceDiscountKey)
        aCoder.encode(recycle, forKey: PropertyKey.recycleKey)
        aCoder.encode(clientQtd, forKey: PropertyKey.clientQtdKey)
        aCoder.encode(clientQtdState, forKey: PropertyKey.clientQtdStateKey)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let prodPhoto = aDecoder.decodeObject(forKey: PropertyKey.prodPhotoKey) as! UIImage
        let prodDescription = aDecoder.decodeObject(forKey: PropertyKey.prodDescriptionKey) as! String
        let boxUnit = aDecoder.decodeObject(forKey: PropertyKey.boxUnitKey) as! String
        let boxDescription = aDecoder.decodeObject(forKey: PropertyKey.boxDescriptionKey) as! String
        let qtdProduct = aDecoder.decodeObject(forKey: PropertyKey.qtdProductkey) as! String
        let qtdDescription = aDecoder.decodeObject(forKey: PropertyKey.qtdDescriptionKey) as! String
        let price = aDecoder.decodeObject(forKey: PropertyKey.priceKey) as! String
        let priceDiscount = aDecoder.decodeObject(forKey: PropertyKey.priceDiscountKey) as! String
        let recycle = aDecoder.decodeObject(forKey: PropertyKey.recycleKey) as! String
        let clientQtd = aDecoder.decodeObject(forKey: PropertyKey.clientQtdKey) as! String
        let clientQtdState = aDecoder.decodeObject(forKey: PropertyKey.clientQtdStateKey) as! String
        
        self.init(prodPhoto: prodPhoto, prodDescription: prodDescription, boxUnit: boxUnit, boxDescription: boxDescription, qtdProduct: qtdProduct, qtdDescription: qtdDescription, price: price, priceDiscount: priceDiscount, recycle: recycle, clientQtd: clientQtd, clientQtdState: clientQtdState)
    }
    
    
    // MARK: - Archiving Paths
      static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
      static let ArchiveURL = DocumentsDirectory.appendingPathComponent("Products")
}
