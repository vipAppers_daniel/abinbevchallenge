//
//  BrowseViewController.swift
//  abinbevChallenge
//
//  Created by Station_02 on 02/08/20.
//  Copyright © 2020 Vip Appers. All rights reserved.
//

import UIKit
import SystemConfiguration

class BrowseViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, finishButtonDelegate, plusButtonDelegate, minusButtonDelegate{
    
// MARK: - Properties
    var products = [Product]()
    
    var emptyImage: UIImage!
    var recycleImage: UIImage!
    var prodWebImage: UIImage!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
// MARK: - View Controllers Life Circle Codes
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emptyImage = UIImage(named: "img_empty")!
        recycleImage = UIImage(named: "img_recycle")!
    
        if let savedProducts = loadProducts() {
            products += savedProducts
        }
        else {
            loadSampleProducts()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
      
    
// MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        products = products.sorted(by: { $0.prodDescription < $1.prodDescription })
    
        let cellIdentifier = "browseCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! BrowseTableViewCell
        
        let product = products[(indexPath as NSIndexPath).row]
        
        cell.cellDelegateFinish = self
        cell.cellDelegatePlus = self
        cell.cellDelegateMinus = self
        
        cell.btnAddProduct.tag = indexPath.row
        cell.btnPlus.tag = indexPath.row
        cell.btnMinus.tag = indexPath.row
        
        cell.productImageView.image = product.prodPhoto
        cell.prodDescriptionLabel.text = product.prodDescription
        cell.boxUnitLabel.text = product.boxUnit
        cell.boxDescriptionLabel.text = product.boxDescription
        cell.qtdProductLabel.text = product.qtdProduct
        cell.qtdDescriptionLabel.text = product.qtdDescription
        
        cell.btnAddProduct.setImage(UIImage(named: "btn_action"), for: .normal)
        cell.btnMinus.isEnabled = true
        cell.btnPlus.isEnabled = true
        
        cell.qtdTextField.backgroundColor = .white
        
        if product.clientQtdState == "on" {
            cell.btnAddProduct.setImage(UIImage(named: "btn_action_done"), for: .normal)
            cell.btnMinus.isEnabled = false
            cell.btnPlus.isEnabled = false
            
            cell.qtdTextField.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.02)
        }
        
        if product.priceDiscount == "" {
            cell.priceLabel.textColor = .black
            cell.priceLabel.text = product.price
            cell.priceLabel.font = UIFont.boldSystemFont(ofSize: 17.0)
            cell.priceDiscount.text = product.priceDiscount
        }
        else {
            cell.priceLabel.text = product.priceDiscount
            cell.priceLabel.textColor = .systemGreen
            
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: product.price)
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
            
            cell.priceDiscount.attributedText = attributeString
        }
        
        cell.recycleImage.image = emptyImage
        
        if product.recycle == "yes" {
            cell.recycleImage.image = recycleImage
        }
        
        cell.qtdTextField.text = product.clientQtd
        return cell
    }
    
    
    // MARK: Navegation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showEdit" {
            let productDetailViewController = segue.destination as! EditViewController
            
            if let selectedProductCell = sender as? BrowseTableViewCell {
                let indexPath = tableView.indexPath(for: selectedProductCell)!
                let selectedProduct = products[(indexPath as NSIndexPath).row]
                productDetailViewController.product = selectedProduct
            }
        }
    }
    
    
    @IBAction func unwindToProductsEdited(_ sender: UIStoryboardSegue) {
        
        if let sourceViewController = sender.source as? EditViewController, let product = sourceViewController.product {
            
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                products[selectedIndexPath.row] = product
                tableView.reloadData()
            }
            
            print(product.prodDescription)
            savedProducts()
        }
    }
    
    
    // MARK: - Delegate and Buttons Actions
    
    @IBAction func refreshAction(_ sender: UIBarButtonItem) {
        products.removeAll()
        loadSampleProducts()
        tableView.reloadData()
        savedProducts()
    }
    
    
    func didPressPlusButton(_ tag: Int) {
        var qtd = Int(products[tag].clientQtd)!
        qtd = qtd + 1
        
        products[tag].clientQtd = "\(qtd)"
        tableView.reloadData()
    }
    
    
    func didPressMinusButton(_ tag: Int) {
        var qtd = Int(products[tag].clientQtd)!
        qtd = qtd - 1
       
        if qtd >= 1 {
            products[tag].clientQtd = "\(qtd)"
        }
        tableView.reloadData()
    }
    
    
    func didPressFinishButton(_ tag: Int) {
        if products[tag].clientQtdState == "off" {
            products[tag].clientQtdState = "on"
        }
        else {
            products[tag].clientQtdState = "off"
        }
        savedProducts()
        tableView.reloadData()
    }
    
        
    
    // MARK: - NSCoding
    func loadSampleProducts() {
        let prodPhoto = UIImage(named: "cell_img_pepsi")!
        let pepsi = Product(prodPhoto: prodPhoto, prodDescription: "Pepsi Lata", boxUnit: "1x1", boxDescription: "Unidade por Caja", qtdProduct: "120ML", qtdDescription: "CAN", price: "RD$10.60", priceDiscount: "RD$10.20", recycle: "no", clientQtd: "1", clientQtdState: "off")!
       
        let prodPhoto1 = UIImage(named: "cell_img_pepsi_ligth")!
        let pepsi_ligth = Product(prodPhoto: prodPhoto1, prodDescription: "PEPSI LIGTH 24/120NZ LATA", boxUnit: "1x24", boxDescription: "Unidade por Caja", qtdProduct: "120ML", qtdDescription: "LATA", price: "RD$603.90", priceDiscount: "", recycle: "no", clientQtd: "1", clientQtdState: "off")!
       
        let prodPhoto2 = UIImage(named: "cell_img_seven_up")!
        let seveUp = Product(prodPhoto: prodPhoto2, prodDescription: "Seven Up lata", boxUnit: "1x24", boxDescription: "Unidade por Caja", qtdProduct: "120ML", qtdDescription: "CAN", price: "RD$380.20", priceDiscount: "RD$200.90", recycle: "yes", clientQtd: "1", clientQtdState: "off")!
        
        if isConnectedToNetwork() {
            let url = URL(string: "https://qa-m1-dr.abi-sandbox.net/media/catalog/product/-/R/-R002151.png")
            let data = try? Data(contentsOf: url!)
            prodWebImage = UIImage(data: data!)
            
            let webProduct = Product(prodPhoto: prodWebImage, prodDescription: "Ron Barceló", boxUnit: "1x1", boxDescription: "Unidade por Caja", qtdProduct: "705ML", qtdDescription: "BOT", price: "RD$500.00", priceDiscount: "", recycle: "no", clientQtd: "1", clientQtdState: "off")!
            
            products += [webProduct, pepsi, pepsi_ligth, seveUp]
        }
        else {
            products += [pepsi, pepsi_ligth, seveUp]
        }
    }
    
    func loadProducts() -> [Product]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Product.ArchiveURL.path) as? [Product]
    }
    
    func savedProducts() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(products, toFile: Product.ArchiveURL.path)
        if !isSuccessfulSave {
            print("Failed to save products...")
        }
    }
    
    
    // MARK: - Internet Connection Check
    
    /*
     In frist time that user open this aplication, check if internet is avalible to download image from url.
    */
    
    func isConnectedToNetwork() -> Bool {
           var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
           zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
           zeroAddress.sin_family = sa_family_t(AF_INET)

           let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
               $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                   SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
               }
           }

           var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
           if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
               return false
           }

           let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
           let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
           let isConnected = (isReachable && !needsConnection)

           return isConnected
       }
}
