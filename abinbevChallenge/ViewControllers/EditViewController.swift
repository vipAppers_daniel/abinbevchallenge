//
//  EditViewController.swift
//  abinbevChallenge
//
//  Created by Station_02 on 02/08/20.
//  Copyright © 2020 Vip Appers. All rights reserved.
//

import UIKit

class EditViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var product: Product?
    
    var intVolum = 0
    var intPrice = 0
    var intDiscountPrice = 0

    @IBOutlet weak var btnSave: UIBarButtonItem!
    
    @IBOutlet weak var photimage: UIImageView!
    
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var boxQtdTextField: UITextField!
    @IBOutlet weak var boxQtdDescriptionTextField: UITextField!
    @IBOutlet weak var bottleVolumTextField: UITextField!
    @IBOutlet weak var bottleTypeTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var discountPriceTextfield: UITextField!
    
    @IBOutlet weak var oneWaySegmented: UISegmentedControl!
    @IBOutlet weak var selectImageButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let product = product {
            
            descriptionTextField.delegate = self
            boxQtdTextField.delegate = self
            boxQtdDescriptionTextField.delegate = self
            bottleVolumTextField.delegate = self
            bottleTypeTextField.delegate = self
            priceTextField.delegate = self
            discountPriceTextfield.delegate = self
            
            selectImageButton.backgroundColor = hexStringToUIColor(hex: "#00239A")
            
            priceTextField.tag = 1
            discountPriceTextfield.tag = 2
            bottleVolumTextField.tag = 3
            
            photimage.image = product.prodPhoto
            descriptionTextField.text = product.prodDescription
            boxQtdTextField.text = product.boxUnit
            boxQtdDescriptionTextField.text = product.boxDescription
            
            bottleTypeTextField.text = product.qtdDescription
            
            var volum = product.qtdProduct.replacingOccurrences(of: "ML", with: "")
            volum = volum.replacingOccurrences(of: ".", with: "")
            volum = volum.replacingOccurrences(of: ",", with: "")
            
            intVolum = Int(volum)!
            bottleVolumTextField.text = updateAmoutVolum(amt: intVolum)
            
            var price = product.price.replacingOccurrences(of: "RD$", with: "")
            price = price.replacingOccurrences(of: ".", with: "")
            price = price.replacingOccurrences(of: ",", with: "")

            intPrice = Int(price)!
            priceTextField.text = updateAmout(amt: intPrice)
            
            var discountPrice = product.priceDiscount.replacingOccurrences(of: "RD$", with: "")
            discountPrice = discountPrice.replacingOccurrences(of: ".", with: "")
            discountPrice = discountPrice.replacingOccurrences(of: ",", with: "")

            if discountPrice != "" {
                intDiscountPrice = Int(discountPrice)!
                discountPriceTextfield.text = updateAmout(amt: intDiscountPrice)
            }
            else {
                 discountPriceTextfield.text = ""
            }
            
            let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(EditViewController.doneClick))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(EditViewController.cancelClick))
            
            let toolBar = UIToolbar()
            toolBar.barStyle = .default
            toolBar.isTranslucent = true
            toolBar.tintColor = UIColor(red: 0, green: 0, blue: 255/255, alpha: 1)
            toolBar.sizeToFit()
            toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
            toolBar.isUserInteractionEnabled = true
            
            bottleVolumTextField.inputAccessoryView = toolBar
            priceTextField.inputAccessoryView = toolBar
            discountPriceTextfield.inputAccessoryView = toolBar
            
            let oneWay = product.recycle
            
            if oneWay == "yes" {
                oneWaySegmented.selectedSegmentIndex = 0
            }
            else {
                oneWaySegmented.selectedSegmentIndex = 1
            }
            
            btnSave.isEnabled = false
            enableSaveButton()
        }
    }
    
    
    //MARK: UIImagePickerControllerDelegate
    @IBAction func selectImageFromPhotoLibrary(_ sender: Any) {
        descriptionTextField.resignFirstResponder()
        let imagePickerController = UIImagePickerController()
    
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following:\(info)")
        }
        photimage.image = selectedImage
        dismiss(animated: true, completion: nil)
    }
    
    
    
    //MARK: Actions
    @IBAction func setRecycle(_ sender: UISegmentedControl) {
        switch oneWaySegmented.selectedSegmentIndex {
        case 0:
            product?.recycle = "yes"
        case 1:
            product?.recycle = "no"
        default:
            break
        }
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        btnSave.isEnabled = false
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField.tag {
           
        case 1:
            if priceTextField.text!.count > 9 {}
            else if let digit = Int(string) {
                intPrice = intPrice * 10 + digit
                priceTextField.text = updateAmout(amt: intPrice)!
            }
            if string == "" {
                intPrice = intPrice/10
                priceTextField.text = updateAmout(amt: intPrice)!
            }
            return false
            
        case 2:
            if discountPriceTextfield.text!.count > 9 {}
            else if let digit = Int(string) {
                intDiscountPrice = intDiscountPrice * 10 + digit
                discountPriceTextfield.text = updateAmout(amt: intDiscountPrice)!
            }
            if string == "" {
                intDiscountPrice = intDiscountPrice/10
                discountPriceTextfield.text = updateAmout(amt: intDiscountPrice)!
            }
            if discountPriceTextfield.text == "0.00" || discountPriceTextfield.text == "0,00"{
                discountPriceTextfield.text = ""
            }
            return false
            
        case 3:
            if bottleVolumTextField.text!.count > 5 {}
            else if let digit = Int(string) {
                intVolum = intVolum * 10 + digit
                bottleVolumTextField.text = updateAmoutVolum(amt: intVolum)
            }
            if string == "" {
                intVolum = intVolum/10
                bottleVolumTextField.text = updateAmoutVolum(amt: intVolum)
            }
            if bottleVolumTextField.text == "0" {
                bottleVolumTextField.text = ""
            }
            return false
            
        default:
             return true
        }
    }
    
    func updateAmoutVolum(amt: Int) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        formatter.minimumFractionDigits = 0
        
        return formatter.string(from: NSNumber(value: amt))!
    }
    
    
    
    func updateAmout(amt: Int) -> String? {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        formatter.minimumFractionDigits = 2
        
        let amount = Double(amt/100) + Double(amt%100)/100
        return formatter.string(from: NSNumber(value: amount))!
    }
    
    
    @objc func doneClick() {
        cancelClick()
    }
    
    
    @objc func cancelClick() {
        bottleVolumTextField.resignFirstResponder()
        priceTextField.resignFirstResponder()
        discountPriceTextfield.resignFirstResponder()
        enableSaveButton()
    }
    
   
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        enableSaveButton()
        return true
    }
    
    
    func enableSaveButton() {
        if descriptionTextField.text != "" && boxQtdTextField.text != "" && boxQtdDescriptionTextField.text != "" && bottleVolumTextField.text != "" && bottleTypeTextField.text != "" && priceTextField.text != "" && priceTextField.text != "0.00" {
            btnSave.isEnabled = true
        }
        else {
            btnSave.isEnabled = false
        }
    }
    
    
    @IBAction func save(_ sender: Any) {
        performSegue(withIdentifier: "returnEdited", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let prodPhoto = photimage.image!
        let prodDescription = descriptionTextField.text?.uppercased()
        let boxUnit = boxQtdTextField.text
        let boxDescription = boxQtdDescriptionTextField.text
        let qtdProduct = bottleVolumTextField.text! + "ML"
        let qtdDescription = bottleTypeTextField.text?.uppercased()
        let price = "RD$" + priceTextField.text!
        
        var priceDiscount = ""
        
        if discountPriceTextfield.text! != "" {
            priceDiscount = "RD$" + discountPriceTextfield.text!
        }
        
        product = Product(prodPhoto: prodPhoto, prodDescription: prodDescription!, boxUnit: boxUnit!, boxDescription: boxDescription!, qtdProduct: qtdProduct, qtdDescription: qtdDescription!, price: price, priceDiscount: priceDiscount, recycle: product!.recycle, clientQtd: product!.clientQtd, clientQtdState: product!.clientQtdState)!
    }
    
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}
