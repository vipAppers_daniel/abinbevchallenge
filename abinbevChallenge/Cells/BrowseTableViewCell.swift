//
//  BrowseTableViewCell.swift
//  abinbevChallenge
//
//  Created by Station_02 on 01/08/20.
//  Copyright © 2020 Vip Appers. All rights reserved.
//

import UIKit

protocol finishButtonDelegate : class {
    func didPressFinishButton(_ tag: Int)
}

protocol plusButtonDelegate : class {
    func didPressPlusButton(_ tag: Int)
}

protocol minusButtonDelegate : class {
    func didPressMinusButton(_ tag: Int)
}


class BrowseTableViewCell: UITableViewCell {
    
    var cellDelegateFinish: finishButtonDelegate?
    var cellDelegatePlus: plusButtonDelegate?
    var cellDelegateMinus: minusButtonDelegate?
    
    @IBOutlet weak var productImageView: UIImageView!
    
    @IBOutlet weak var prodDescriptionLabel: UILabel!
    
    @IBOutlet weak var boxUnitLabel: UILabel!
    @IBOutlet weak var boxDescriptionLabel: UILabel!
    
    @IBOutlet weak var qtdProductLabel: UILabel!
    @IBOutlet weak var qtdDescriptionLabel: UILabel!
    
    @IBOutlet weak var recycleImage: UIImageView!
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceDiscount: UILabel!

    @IBOutlet weak var qtdTextField: UITextField!
    
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnAddProduct: UIButton!
    
    
    @IBAction func btnFinishPressed(_ sender: UIButton) {
        cellDelegateFinish?.didPressFinishButton(sender.tag)
    }
    
    @IBAction func btnPlus(_ sender: UIButton) {
        cellDelegatePlus?.didPressPlusButton(sender.tag)
    }
    
    @IBAction func btnMinus(_ sender: UIButton) {
        cellDelegateMinus?.didPressMinusButton(sender.tag)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}


